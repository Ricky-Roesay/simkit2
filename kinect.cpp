#include "kinect.hpp"
#include "util.h"
#include <opencv2/imgcodecs.hpp>
#include <opencv2/highgui.hpp>
#include <iostream>
#include <chrono>
#include <fstream>
#include <string>


// Constructor
kinect::kinect( const uint32_t index )
    : device_index( index )
{
    // Initialize
    initialize();
}

kinect::~kinect()
{
    // Finalize
    finalize();
}

// Initialize
void kinect::initialize()
{
    // Initialize Sensor
    initialize_sensor();

    // Initialize Viewer
    initialize_viewer();

    start_recording = 0;
    show_video = 0;
    photo_num = 0;
}

// Initialize Sensor
inline void kinect::initialize_sensor()
{
    // Get Connected Devices
    const int32_t device_count = k4a::device::get_installed_count();
    if( device_count == 0 ){
        throw k4a::error( "Failed to found device!" );
    }

    // Open Default Device
    device = k4a::device::open( device_index );

    // Start Cameras with Configuration
    device_configuration = K4A_DEVICE_CONFIG_INIT_DISABLE_ALL;
    device_configuration.color_format             = k4a_image_format_t::K4A_IMAGE_FORMAT_COLOR_BGRA32;
    device_configuration.color_resolution         = k4a_color_resolution_t::K4A_COLOR_RESOLUTION_720P;
    device_configuration.depth_mode               = k4a_depth_mode_t::K4A_DEPTH_MODE_NFOV_UNBINNED;
    device_configuration.synchronized_images_only = true;
    device_configuration.wired_sync_mode          = k4a_wired_sync_mode_t::K4A_WIRED_SYNC_MODE_STANDALONE;
    device.start_cameras( &device_configuration );

    // Get Calibration
    calibration = device.get_calibration( device_configuration.depth_mode, device_configuration.color_resolution );

    // Create Transformation
    transformation = k4a::transformation( calibration );
}

// Initialize Viewer
inline void kinect::initialize_viewer()
{
    #ifdef HAVE_OPENCV_VIZ
    // Create Viewer
    const cv::String window_name = cv::format( "point cloud (kinect %d)", device_index );
    viewer = cv::viz::Viz3d( window_name );

    // Show Coordinate System Origin
    constexpr double scale = 100.0;
    viewer.showWidget( "origin", cv::viz::WCameraPosition( scale ) );
    #endif
}

// Finalize
void kinect::finalize()
{
    // Destroy Transformation
    transformation.destroy();

    // Stop Cameras
    device.stop_cameras();

    // Close Device
    device.close();

    // Close Window
    cv::destroyAllWindows();

    #ifdef HAVE_OPENCV_VIZ
    // Close Viewer
    viewer.close();
    #endif
}

// Run
void kinect::run()
{
    // Main Loop
    while( true ){
        // Update
        update();

        // Draw
        draw();

        // Show
        show();

        // Wait Key
        constexpr int32_t delay = 30;
        const int32_t key = cv::waitKey( delay );
        if( key == 'q' ){
            break;
        }

        #ifdef HAVE_OPENCV_VIZ
        if( viewer.wasStopped() ){
            break;
        }
        #endif
    }
}


///////////////////// Update
inline void kinect::update()
{

    // Get Capture Frame
    constexpr std::chrono::milliseconds time_out( K4A_WAIT_INFINITE );
    const bool result = device.get_capture( &capture, time_out );
    if( !result ){
        this->~kinect();
    }

    color_image = capture.get_color_image();  // Get Color Image

    depth_image = capture.get_depth_image(); // Get Depth Image
    
    ir_image = capture.get_ir_image();  // yep its ir image time



    if( !depth_image.handle() ){ return; }  
    //transformed_depth_image = transformation.depth_image_to_color_camera( depth_image ); 
            //Transform Depth Image to Color Camera

    //if( !transformed_depth_image.handle() ){ return; }
    //xyz_image = transformation.depth_image_to_point_cloud( transformed_depth_image, K4A_CALIBRATION_TYPE_COLOR );
            // Transform Depth Image to Point Cloud

    capture.reset(); // Release Capture Handle
}


///////////////////// Draw
inline void kinect::draw()
{   
    if( !color_image.handle() ){ return; }
    color = k4a::get_mat( color_image ); // Get cv::Mat from k4a::image
    color_image.reset(); // Release Color Image Handle

    if( !depth_image.handle() ){ return; } 
    depth = k4a::get_mat( depth_image ); // Get cv::Mat from k4a::image
    depth_image.reset(); // Release Depth Image Handle

    //if( !transformed_depth_image.handle() ){ return; } 
    //transformed_depth = k4a::get_mat( transformed_depth_image ); // Get cv::Mat from k4a::imag
    //transformed_depth_image.reset(); // Release Transformed Image Handle

    if( !ir_image.handle() ){ return; } 
    ir = k4a::get_mat( ir_image );  // Get cv::Mat from k4a::image
    ir_image.reset();  // Release Point Cloud Image Handle

}


///////////////////// Show
inline void kinect::show()
{
    // /*
    static std::string line;
    std::ifstream myfile;
    std::ofstream myfile2;
    /*
    *  the bottom chunk of code in the if statement should be replaced to 
        help transfer file number indexes and boolean statements in a much more efficient manner

        Or just do everything in a CPP application
    *
    */
    myfile.open("/home/cow-go-moo/PycharmProjects/test/status_file.txt", std::ios::in);
    if (myfile.is_open())
    {
        getline(myfile,line);
        int f1 = line.find(".");
        to_upload = line.substr(0,f1);
        line = line.substr((f1+1));
        int f2 = line.find(".");
        //to_record = line.substr((f1+1),f2);
        //std::cout << f2 << std::endl;
        record_str = line[f2+1];
        video_str = line[f2+3];
        upload_str = line[f2+5];
        //std::cout << line << std::endl;  //////////shorter line

        start_recording = std::stoi(record_str);
        show_video = std::stoi(video_str);

        //std::cout << start_recording << " " << show_video << std::endl; //// x x 
        line = to_upload + "." + std::to_string(photo_num)+ "." +record_str + "." +video_str + "." +upload_str;
        //std::cout << line << std::endl;   ///////////////  line longer
        myfile.close();
        myfile2.open("/home/cow-go-moo/PycharmProjects/test/status_file.txt", std::ios::out | std::ios::trunc);
        if (myfile2.is_open())
            myfile2.write(line.c_str(), line.length());
        myfile.close();
    }

    
    if( color.empty() || ir.empty() || depth.empty() ){ return; }

    if(show_video)
    {
        const cv::String window_name = cv::format( "color (kinect %d)", device_index ); // Show Image
        cv::imshow( window_name, color );
    }

    if(start_recording){
        photo_num_str = std::to_string(photo_num) + ".jpg";
        ir_name = "/media/cow-go-moo/removable-SSD/_ir" + photo_num_str;
        depth.convertTo( depth, CV_8U, -255.0 / 5000.0, 255.0 );
        depth_name = "/media/cow-go-moo/removable-SSD/_depth" + photo_num_str;
        color_name = "/media/cow-go-moo/removable-SSD/_color" + photo_num_str;
        imwrite(ir_name, ir);
        imwrite(depth_name, depth);
        imwrite(color_name, color);
        photo_num++;
    }
    


    
    //ir.convertTo( ir, CV_8U, 0.5 );
    //const cv::String window_name2 = cv::format( "ir (kinect %d)", device_index ); // Show Image
    //cv::imshow( window_name2, ir );

    /*
    cv::Mat depth2 = imread('/media/cow-go-moo/remo')

    depth.convertTo( depth, CV_8U, -255.0 / 5000.0, 255.0 );
    const cv::String window_name3 = cv::format( "depth (kinect %d)", device_index ); // Show Image
    cv::imshow( window_name3, ir );
     

    //transformed_depth.convertTo( transformed_depth, CV_8U, -255.0 / 5000.0, 255.0 ); // Scaling Depth

    //const cv::String window_name2 = cv::format( "transformed depth (kinect %d)", device_index ); // Show Image
    //cv::imshow( window_name2, transformed_depth );

    #ifdef HAVE_OPENCV_VIZ
    // Create Point Cloud Widget
    cv::viz::WCloud cloud = cv::viz::WCloud( xyz, color );

    // Show Widget
    viewer.showWidget( "cloud", cloud );
    viewer.spinOnce();
    #endif
    */

}

