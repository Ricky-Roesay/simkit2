import sys
sys.path.append('/opt/nvidia/jetson-gpio/lib/python')   # this is for Jetson.GPIO module
sys.path.append('/usr/local/lib/python3.6/dist-packages')  # this is for PIL module
sys.path.append('/home/cow-go-moo/.local/lib/python3.6/site-packages')   # this is for pydrive

import Jetson.GPIO
from PIL import Image, ImageTk
from pydrive.auth import GoogleAuth
from pydrive.drive import GoogleDrive

from tkinter import *
import time
import os

window = Tk()
output_text = StringVar()
record_time = StringVar()

gauth = GoogleAuth()
gauth.LocalWebserverAuth()
drive = GoogleDrive(gauth)

prompt_delete = 0
confirm_delete = 0

status_file_lines = []
## 0 upload index  (memory concerns)
## 1 delete index
## 2 record? 0 1
## 3 show video? 0 1
## 4 upload to drive? 0 1

# TO ANYONE LOOKING AT THIS, DONT USE A TXT FILE TO TRANSFER 5 VARIABLES
# BETWEEN A PYTHON SCRIPT AND A C++ APP!!!

# TERRIBLE WAY TO DO THIS, I WROTE THIS AT 3am

def read_status_file():
    status_file = open('status_file.txt', 'r+')
    line = status_file.read()  # read from status file
    new_list = line.split(".")
    for s in new_list:
        #status_file_lines[count] = s
        status_file_lines.append(int(s))
    status_file.close()


read_status_file()
#os.os.system("sudo rm _*")  ########## START C++ APPLICATION

def reset_status_file():
    f = open("status_file.txt", "w")
    f.write("0.0.0.0.0")
    f.close()

def set_status_file():
    status_file = open('status_file.txt', 'r+')
    temp_tine = status_file.read()  # read from status file
    new_lines = temp_tine.split(".")
    if len(new_lines) < 3:
        status_file.close()
        set_status_file()
    else:
        temp_int = int(new_lines[1])
        #print(temp_int)
        status_file_lines[1] = temp_int
        status_file.close()
        status_file = open('status_file.txt', 'w')
        for i in range(0,4):
            status_file.write(str(status_file_lines[i])+".")
        status_file.write(str(status_file_lines[4]))
        status_file.close()
    return  ##  I RECURSIVELY CALLED THIS FUNCTION TO GET PASSED FILE READ ISSUES

def upload_next_capture():
    depth_name = "_depth" + str(status_file_lines[0]) + ".jpg"
    ir_name = "_ir" + str(status_file_lines[0]) + ".jpg"
    color_name = "_color" + str(status_file_lines[0]) + ".jpg"

    depth_location = "/media/cow-go-moo/removable-SSD/" + depth_name
    ir_location = "/media/cow-go-moo/removable-SSD/" + ir_name
    color_location = "/media/cow-go-moo/removable-SSD/" + color_name

    c_file = drive.CreateFile({'title': depth_name})
    c_file.SetContentFile(depth_location)
    c_file.Upload()  # Upload the file.

    c_file = drive.CreateFile({'title': ir_name})
    c_file.SetContentFile(ir_location)
    c_file.Upload()  # Upload the file.

    c_file = drive.CreateFile({'title': color_name})
    c_file.SetContentFile(color_location)
    c_file.Upload()  # Upload the file.
    os.chdir('/media/cow-go-moo/removable-SSD')
    os.system("rm "+ depth_name)
    os.system("rm " + ir_name)
    os.system("rm " + color_name)
    os.chdir('/home/cow-go-moo/PycharmProjects/test')  ## this might get changed later

    status_file_lines[0] += 1


def update_record():
    temp_text = ""
    if status_file_lines[2] == 1 and status_file_lines[4] == 1:
        temp_text = "Uploading footage"
        status_file_lines[2] = 0
        but_record.text = " START RECORDING "
        output_text.set(temp_text)
        window.update()
    elif status_file_lines[2] == 1 and status_file_lines[4] == 0:  # wasnt uploading
        temp_text = "Footage not uploading or recording"
        status_file_lines[2] = 0
        but_record.text = " START RECORDING "
        output_text.set(temp_text)
        window.update()
    elif status_file_lines[2] == 0 and status_file_lines[4] == 1:  # already recording
        temp_text = "Recording and uploading footage"
        status_file_lines[2] = 1
        but_record.text = " STOP RECORDING "
        output_text.set(temp_text)
        window.update()
    else:
        temp_text = "Recording footage"
        status_file_lines[2] = 1
        but_record.text = " STOP RECORDING "
        output_text.set(temp_text)
        window.update()
    set_status_file()
    #set_status_file()
    #set_status_file()

def update_upload():
    temp_text = ""
    if status_file_lines[4] == 1 and status_file_lines[2] == 1:
        temp_text = "Recording footage"
        status_file_lines[4] = 0
        but_upload.text = " START UPLOADING "
        output_text.set(temp_text)
        window.update()
    elif status_file_lines[4] == 1 and status_file_lines[2] == 0:  # wasnt recording
        temp_text = "Footage not uploading or recording"
        status_file_lines[4] = 0
        but_upload.text = " START UPLOADING "
        output_text.set(temp_text)
        window.update()
    elif status_file_lines[4] == 0 and status_file_lines[2] == 1:  # already recording
        temp_text = "Recording and uploading footage"
        status_file_lines[4] = 1
        but_upload.text = " STOP UPLOADING "
        output_text.set(temp_text)
        window.update()
    else:  # was not recording either
        temp_text = "Uploading footage"
        status_file_lines[4] = 1
        but_upload.text = " STOP UPLOADING "
        output_text.set(temp_text)
        window.update()
    set_status_file()

def update_show_video():
    temp_text = ""
    if status_file_lines[3] == 1:
        temp_text = "Video output off"
        status_file_lines[3] = 0
        but_viewer.text = " SHOW VIDEO "
        output_text.set(temp_text)
        window.update()
    else:
        temp_text = "Video output on"
        status_file_lines[3] = 1
        but_viewer.text = " HIDE VIDEO "
        output_text.set(temp_text)
        window.update()
    set_status_file()
    time.sleep(1)


def GPIO_test(channel):
    print(channel)
    print("did it actually fucking work?")
    #GPIO.add_event_detect(12, GPIO.RISING, callback=GPIO_test, bouncetime=10)


def prompt_delete():
    temp_text = "Press the confirm button on the screen to delete the files"
    output_text.set(temp_text)
    window.update()
    global prompt_delete
    prompt_delete = 1

def confirm_delete():
    temp_text = "Deleting files..."
    output_text.set(temp_text)
    window.update()
    global confirm_delete
    confirm_delete = 1

def delete_files():
    os.chdir('/media/cow-go-moo/removable-SSD')
    os.system("rm _*")
    os.chdir('/home/cow-go-moo/PycharmProjects/test')  ## this might get changed later
    status_file_lines[0] = 0
    status_file_lines[1] = 0
    reset_status_file()
    #reset_status_file()
    global prompt_delete
    prompt_delete = 0
    global confirm_delete
    confirm_delete = 0


window.title("Piggy goes oink")

window.geometry('1024x600')

bg = ImageTk.PhotoImage(file = "pig.jpg")
img_label =  Label( window, image = bg)
img_label.place(x = 0, y = 0)

frame1 = Frame(width=500, height=500, background="white")
frame1.place(in_=window, anchor="c", relx=.5, rely=.5)

Label(frame1, textvariable=output_text, background="white").grid(row=0, columnspan = 3)

but_record = Button(frame1, text=" START/STOP RECORDING ", background="white", command=update_record)
but_upload = Button(frame1, text=" START/STOP UPLOADING ", background="white", command=update_upload)
but_viewer = Button(frame1, text=" TOGGLE VIDEO ", background="white", command=update_show_video)
but_prompt = Button(frame1, text=" DELETE FILES ", background="white", command=prompt_delete)
but_confirm = Button(frame1, text=" CONFIRM ", background="white", command=confirm_delete)
but_record.grid(row=1, column=0)
but_upload.grid(row=1, column=1)
but_viewer.grid(row=1, column=2)
but_prompt.grid(row=2, columnspan = 3)
but_confirm.grid(row=3, columnspan = 3)

#window.attributes("-fullscreen", True)  # substitute `Tk` for whatever your `Tk()` object is called


#def main():

#  11, 12, 13, 21, 23
record = 11
upload = 12
viewer = 13
delete = 21
confirm = 23
#gpio pin 12 works as well.
Jetson.GPIO.setmode(Jetson.GPIO.BOARD)  # BOARD pin-numbering scheme
Jetson.GPIO.setup(record, Jetson.GPIO.IN)
Jetson.GPIO.setup(upload, Jetson.GPIO.IN)
Jetson.GPIO.setup(viewer, Jetson.GPIO.IN)
Jetson.GPIO.setup(delete, Jetson.GPIO.IN)
Jetson.GPIO.setup(confirm, Jetson.GPIO.IN)

#Jetson.GPIO.add_event_detect(record, Jetson.GPIO.RISING, callback=update_record, bouncetime=10)
#Jetson.GPIO.add_event_detect(upload, Jetson.GPIO.RISING, callback=update_upload, bouncetime=10)
#Jetson.GPIO.add_event_detect(viewer, Jetson.GPIO.RISING, callback=update_show_video, bouncetime=10)
#Jetson.GPIO.add_event_detect(delete, Jetson.GPIO.RISING, callback=prompt_delete, bouncetime=10)
#Jetson.GPIO.add_event_detect(confirm, Jetson.GPIO.RISING, callback=confirm_delete, bouncetime=10)
#  you need <3.3k ohm pull down resistors for it to work, idk why (internal resistance is low?)

def get_inputs():
    #buttons = [0,0,0,0,0]

    b1 = Jetson.GPIO.input(record)
    b2 = Jetson.GPIO.input(upload)
    b3 = Jetson.GPIO.input(viewer)
    b4 = Jetson.GPIO.input(delete)
    b5 = Jetson.GPIO.input(confirm)
    #print(b1,' ', b2,' ', b3,' ', b4,' ',b5)
    if b1 == 1:
        update_record()
    elif b2 == 1:
        update_upload()
    elif b3 == 1:
        update_show_video()
    elif b4 == 1:
        prompt_delete()
    elif b5 == 1:
        update_show_video()
    else:
        return
    time.sleep(1)


def main():
    try:
        global prompt_delete
        global confirm_delete
        temp_text = "Loading Program"
        output_text.set(temp_text)
        window.update()
        time.sleep(3)
        while 1:
            ############ DELETE FILES ###########
            if prompt_delete == 1:
                for i in range(1,500):
                    window.update_idletasks()
                    #get_inputs()
                    window.update()
                    time.sleep(0.01)
                if confirm_delete == 1:
                    delete_files()
                    temp_text = "Deleting files..."
                    output_text.set(temp_text)
                    window.update()
                    time.sleep(1)
                    temp_text = "Files Deleted. Resuming program"
                    output_text.set(temp_text)
                    window.update()
                else:
                    prompt_delete = 0
                    temp_text = "Files not deleted"
                    output_text.set(temp_text)
                    window.update()
            ############ DELETE FILES ###########

            if status_file_lines[4] == 1:
                if status_file_lines[1] == status_file_lines[0]:
                    temp_text = "Warning!!! There are no more files to upload"
                    output_text.set(temp_text)
                    window.update()
                else:
                    upload_next_capture()
            get_inputs()
            window.update_idletasks()
            window.update()
    finally:
        print("application ended")
        #Jetson.GPIO.cleanup()  # cleanup all GPIOs
        set_status_file()

if __name__ == '__main__':
    main()
